package main

import (
	"fmt"
)

func main() {
	fmt.Println("hello")

	face := Face{}
	//a:= []string{"w","3","d","b"}

	//fmt.Println(pos1(a))
	fmt.Println("please place cube in the way, yellow top, red on the left, green on the front \n" +
		"orange on the right, blue on the back and white on the bottom")

	fmt.Println("Enter the cube colours going from left to right")
	fmt.Println(face.fullCube())
	fmt.Scan()
	//
	fmt.Println(face.whiteCross())
	fmt.Println(face.whiteFaceBottom())
	fmt.Println(face.F2L())
	fmt.Println(face.topCross())
	fmt.Println(face.topFaceFull())

}

func pos1(value []string) int {
	for p, x := range value {
		if value[p] == x {
			return p
		}
	}
	return -1
}

func createFaceArray() {
	n := 0
	fmt.Scan(&n)
	//create a 2d array, n is the number of arrays in there
	slice := make([][]int, 0, n)
	for i := 0; i < n; i++ {
		//create an array in each on
		row := make([]int, n)
		//ask for an input inside each element for an array
		for j, _ := range row {
			fmt.Scan(&row[j])
		}
		slice = append(slice, row)
	}

	fmt.Println(slice)
}
func set(face int) {
	a := make([]int, face)
	for i := 0; i < face; i++ {
		fmt.Scan(&a[i])
	}
	fmt.Println(a)
}
