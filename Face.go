package main

import (
	"fmt"
)

type Face struct {
	whiteFace [][]string

	yellowFace [][]string

	greenFace [][]string

	blueFace [][]string

	redFace [][]string

	orangeFace [][]string

	topFrontCorners []string

	bottomRowCorners []string

	topFaceCorners []string

	topFrontCentres []string

	topandFrontFaceCentres [][]string

	fullcube [][][]string
}

func (face *Face) returnCube() [][][]string {

	return face.fullcube

}
func (face *Face) setFrontTopRow() {

	face.topFrontCorners = []string{face.fullcube[1][0][0], face.fullcube[1][0][2], face.fullcube[3][0][0], face.fullcube[3][0][2],
		face.fullcube[5][0][0], face.fullcube[5][0][2], face.fullcube[2][0][0], face.fullcube[2][0][2]}

}

func (face *Face) setFrontTopCentres() {

	face.topFrontCentres = []string{face.fullcube[1][0][1], face.fullcube[3][0][1], face.fullcube[5][0][1],
		face.fullcube[2][0][1]}
}

func (face *Face) setTopAndFrontTopCentres() {

	face.topandFrontFaceCentres = [][]string{
		{face.fullcube[1][0][1], face.fullcube[0][2][1]},
		{face.fullcube[2][0][1], face.fullcube[0][1][0]},
		{face.fullcube[5][0][1], face.fullcube[0][0][1]},
		{face.fullcube[3][0][1], face.fullcube[0][1][2]},
	}

}

func (face *Face) setBottomRow() {

	face.bottomRowCorners = []string{face.fullcube[1][2][0], face.fullcube[1][2][2], face.fullcube[3][2][0], face.fullcube[3][2][2],
		face.fullcube[5][2][0], face.fullcube[5][2][2], face.fullcube[2][2][0], face.fullcube[2][2][2]}

}

func (face *Face) setTopFaceCorners() {

	face.topFaceCorners = []string{face.fullcube[0][0][0], face.fullcube[0][0][2], face.fullcube[0][2][0], face.fullcube[0][2][2]}

}
func (face *Face) updateAllAreas() {
	face.setFrontTopRow()
	face.setBottomRow()
	face.setTopFaceCorners()
	face.setTopAndFrontTopCentres()
	face.setFrontTopCentres()
}

func (face *Face) createWhiteArray() [][]string {
	fmt.Println("what does the white face look like")
	n := 3
	//create a 2d array, n is the number of arrays in there
	slice := make([][]string, 0, n)
	for i := 0; i < n; i++ {
		//create an array in each on
		row := make([]string, n)
		//ask for an input inside each element for an array
		for j, _ := range row {
			fmt.Scan(&row[j])
		}

		slice = append(slice, row)

	}
	face.whiteFace = slice

	return face.whiteFace
}

func (face *Face) createBlueArray() [][]string {
	fmt.Println("what does the blue face look like")
	n := 3
	//create a 2d array, n is the number of arrays in there
	slice := make([][]string, 0, n)
	for i := 0; i < n; i++ {
		//create an array in each on
		row := make([]string, n)
		//ask for an input inside each element for an array
		for j, _ := range row {
			fmt.Scan(&row[j])
		}

		slice = append(slice, row)

	}
	face.blueFace = slice

	return face.blueFace
}

func (face *Face) createRedArray() [][]string {
	fmt.Println("what does the red face look like")
	n := 3
	//create a 2d array, n is the number of arrays in there
	slice := make([][]string, 0, n)
	for i := 0; i < n; i++ {
		//create an array in each on
		row := make([]string, n)
		//ask for an input inside each element for an array
		for j, _ := range row {
			fmt.Scan(&row[j])
		}

		slice = append(slice, row)

	}
	face.redFace = slice

	return face.redFace
}

func (face *Face) createGreenArray() [][]string {
	fmt.Println("what does the green face look like")
	n := 3
	//create a 2d array, n is the number of arrays in there
	slice := make([][]string, 0, n)
	for i := 0; i < n; i++ {
		//create an array in each on
		row := make([]string, n)
		//ask for an input inside each element for an array
		for j, _ := range row {
			fmt.Scan(&row[j])
		}

		slice = append(slice, row)

	}
	face.greenFace = slice

	return face.greenFace
}

func (face *Face) createYellowArray() [][]string {
	fmt.Println("what does the yellow face look like")
	n := 3
	//create a 2d array, n is the number of arrays in there
	slice := make([][]string, 0, n)
	for i := 0; i < n; i++ {
		//create an array in each on
		row := make([]string, n)
		//ask for an input inside each element for an array
		for j, _ := range row {
			fmt.Scan(&row[j])
		}

		slice = append(slice, row)

	}
	face.yellowFace = slice

	return face.yellowFace
}

func (face *Face) createOrangeArray() [][]string {
	fmt.Println("what does the orange face look like")
	n := 3
	//create a 2d array, n is the number of arrays in there
	slice := make([][]string, 0, n)
	for i := 0; i < n; i++ {
		//create an array in each on
		row := make([]string, n)
		//ask for an input inside each element for an array
		for j, _ := range row {
			fmt.Scan(&row[j])
		}

		slice = append(slice, row)

	}
	face.orangeFace = slice

	return face.orangeFace
}

func (face *Face) fullCube() [][][]string {
	//Up/white

	//front/green

	//right/red
	//left/orange
	//down/yellow

	//back/blue

	var yellowCube = face.createYellowArray()
	var greenCube = face.createGreenArray()
	var redCube = face.createRedArray()
	var whiteCube = face.createWhiteArray()
	var blueCube = face.createBlueArray()
	var orangecube = face.createOrangeArray()

	//top		//front	  //left  	//right		//bottom	//back
	dcube := [][][]string{yellowCube, greenCube, redCube, orangecube, whiteCube, blueCube}

	face.fullcube = dcube
	face.updateAllAreas()

	return face.fullcube
}

func (face *Face) pos(value string) string {
	cube := []string{}
	for _, x := range cube {
		if x == value {
			return x
		}
	}
	return ""
}

func pos2(value string) int {
	cube := []string{}
	for i, _ := range cube {
		if cube[i] == value {
			return i
		}
	}
	return -1
}

func (face *Face) rotatetop() [][][]string {

	fmt.Println("rotating top")

	//rotating top
	//each step will replace each 3d array part
	temp := face.fullcube[0][0][0]
	//e.g. 020 will become the first part at the top
	face.fullcube[0][0][0] = face.fullcube[0][2][0]
	face.fullcube[0][2][0] = face.fullcube[0][2][2]
	face.fullcube[0][2][2] = face.fullcube[0][0][2]
	face.fullcube[0][0][2] = temp

	temp = face.fullcube[0][0][1]
	face.fullcube[0][0][1] = face.fullcube[0][1][0]
	face.fullcube[0][1][0] = face.fullcube[0][2][1]
	face.fullcube[0][2][1] = face.fullcube[0][1][2]
	face.fullcube[0][1][2] = temp

	temp = face.fullcube[1][0][0]
	face.fullcube[1][0][0] = face.fullcube[3][0][0]
	face.fullcube[3][0][0] = face.fullcube[5][0][0]
	face.fullcube[5][0][0] = face.fullcube[2][0][0]
	face.fullcube[2][0][0] = temp

	temp = face.fullcube[1][0][1]
	face.fullcube[1][0][1] = face.fullcube[3][0][1]
	face.fullcube[3][0][1] = face.fullcube[5][0][1]
	face.fullcube[5][0][1] = face.fullcube[2][0][1]
	face.fullcube[2][0][1] = temp

	temp = face.fullcube[1][0][2]
	face.fullcube[1][0][2] = face.fullcube[3][0][2]
	face.fullcube[3][0][2] = face.fullcube[5][0][2]
	face.fullcube[5][0][2] = face.fullcube[2][0][2]
	face.fullcube[2][0][2] = temp

	return face.fullcube
}

func (face *Face) rotatetopNoPrint() [][][]string {

	//rotating top
	//each step will replace each 3d array part
	temp := face.fullcube[0][0][0]
	//e.g. 020 will become the first part at the top
	face.fullcube[0][0][0] = face.fullcube[0][2][0]
	face.fullcube[0][2][0] = face.fullcube[0][2][2]
	face.fullcube[0][2][2] = face.fullcube[0][0][2]
	face.fullcube[0][0][2] = temp

	temp = face.fullcube[0][0][1]
	face.fullcube[0][0][1] = face.fullcube[0][1][0]
	face.fullcube[0][1][0] = face.fullcube[0][2][1]
	face.fullcube[0][2][1] = face.fullcube[0][1][2]
	face.fullcube[0][1][2] = temp

	temp = face.fullcube[1][0][0]
	face.fullcube[1][0][0] = face.fullcube[3][0][0]
	face.fullcube[3][0][0] = face.fullcube[5][0][0]
	face.fullcube[5][0][0] = face.fullcube[2][0][0]
	face.fullcube[2][0][0] = temp

	temp = face.fullcube[1][0][1]
	face.fullcube[1][0][1] = face.fullcube[3][0][1]
	face.fullcube[3][0][1] = face.fullcube[5][0][1]
	face.fullcube[5][0][1] = face.fullcube[2][0][1]
	face.fullcube[2][0][1] = temp

	temp = face.fullcube[1][0][2]
	face.fullcube[1][0][2] = face.fullcube[3][0][2]
	face.fullcube[3][0][2] = face.fullcube[5][0][2]
	face.fullcube[5][0][2] = face.fullcube[2][0][2]
	face.fullcube[2][0][2] = temp

	return face.fullcube
}

func (face *Face) rotateRight() [][][]string {

	fmt.Println("rotating right side")
	temp := face.fullcube[3][0][0]
	face.fullcube[3][0][0] = face.fullcube[3][2][0]
	face.fullcube[3][2][0] = face.fullcube[3][2][2]
	face.fullcube[3][2][2] = face.fullcube[3][0][2]
	face.fullcube[3][0][2] = temp

	temp = face.fullcube[3][0][1]
	face.fullcube[3][0][1] = face.fullcube[3][1][0]
	face.fullcube[3][1][0] = face.fullcube[3][2][1]
	face.fullcube[3][2][1] = face.fullcube[3][1][2]
	face.fullcube[3][1][2] = temp

	temp = face.fullcube[1][0][2]
	face.fullcube[1][0][2] = face.fullcube[4][0][2]
	face.fullcube[4][0][2] = face.fullcube[5][2][0]
	face.fullcube[5][2][0] = face.fullcube[0][0][2]
	face.fullcube[0][0][2] = temp

	temp = face.fullcube[1][1][2]
	face.fullcube[1][1][2] = face.fullcube[4][1][2]
	face.fullcube[4][1][2] = face.fullcube[5][1][0]
	face.fullcube[5][1][0] = face.fullcube[0][1][2]
	face.fullcube[0][1][2] = temp

	temp = face.fullcube[1][2][2]
	face.fullcube[1][2][2] = face.fullcube[4][2][2]
	face.fullcube[4][2][2] = face.fullcube[5][0][0]
	face.fullcube[5][0][0] = face.fullcube[0][2][2]
	face.fullcube[0][2][2] = temp

	return face.fullcube
}
func (face *Face) rotateRightNoPrint() [][][]string {

	temp := face.fullcube[3][0][0]
	face.fullcube[3][0][0] = face.fullcube[3][2][0]
	face.fullcube[3][2][0] = face.fullcube[3][2][2]
	face.fullcube[3][2][2] = face.fullcube[3][0][2]
	face.fullcube[3][0][2] = temp

	temp = face.fullcube[3][0][1]
	face.fullcube[3][0][1] = face.fullcube[3][1][0]
	face.fullcube[3][1][0] = face.fullcube[3][2][1]
	face.fullcube[3][2][1] = face.fullcube[3][1][2]
	face.fullcube[3][1][2] = temp

	temp = face.fullcube[1][0][2]
	face.fullcube[1][0][2] = face.fullcube[4][0][2]
	face.fullcube[4][0][2] = face.fullcube[5][2][0]
	face.fullcube[5][2][0] = face.fullcube[0][0][2]
	face.fullcube[0][0][2] = temp

	temp = face.fullcube[1][1][2]
	face.fullcube[1][1][2] = face.fullcube[4][1][2]
	face.fullcube[4][1][2] = face.fullcube[5][1][0]
	face.fullcube[5][1][0] = face.fullcube[0][1][2]
	face.fullcube[0][1][2] = temp

	temp = face.fullcube[1][2][2]
	face.fullcube[1][2][2] = face.fullcube[4][2][2]
	face.fullcube[4][2][2] = face.fullcube[5][0][0]
	face.fullcube[5][0][0] = face.fullcube[0][2][2]
	face.fullcube[0][2][2] = temp

	return face.fullcube
}

func (face *Face) rotateLeft() [][][]string {

	fmt.Println("rotating left side")
	temp := face.fullcube[2][0][0]
	face.fullcube[2][0][0] = face.fullcube[2][2][0]
	face.fullcube[2][2][0] = face.fullcube[2][2][2]
	face.fullcube[2][2][2] = face.fullcube[2][0][2]
	face.fullcube[2][0][2] = temp

	temp = face.fullcube[2][0][1]
	face.fullcube[2][0][1] = face.fullcube[2][1][0]
	face.fullcube[2][1][0] = face.fullcube[2][2][1]
	face.fullcube[2][2][1] = face.fullcube[2][1][2]
	face.fullcube[2][1][2] = temp

	temp = face.fullcube[1][0][0]
	face.fullcube[1][0][0] = face.fullcube[0][0][0]
	face.fullcube[0][0][0] = face.fullcube[5][2][2]
	face.fullcube[5][2][2] = face.fullcube[4][0][0]
	face.fullcube[4][0][0] = temp

	temp = face.fullcube[1][1][0]
	face.fullcube[1][1][0] = face.fullcube[0][1][0]
	face.fullcube[0][1][0] = face.fullcube[5][1][2]
	face.fullcube[5][1][2] = face.fullcube[4][1][0]
	face.fullcube[4][1][0] = temp

	temp = face.fullcube[1][2][0]
	face.fullcube[1][2][0] = face.fullcube[0][2][0]
	face.fullcube[0][2][0] = face.fullcube[5][0][2]
	face.fullcube[5][0][2] = face.fullcube[4][2][0]
	face.fullcube[4][2][0] = temp

	return face.fullcube
}

func (face *Face) rotateLeftNoPrint() [][][]string {

	temp := face.fullcube[2][0][0]
	face.fullcube[2][0][0] = face.fullcube[2][2][0]
	face.fullcube[2][2][0] = face.fullcube[2][2][2]
	face.fullcube[2][2][2] = face.fullcube[2][0][2]
	face.fullcube[2][0][2] = temp

	temp = face.fullcube[2][0][1]
	face.fullcube[2][0][1] = face.fullcube[2][1][0]
	face.fullcube[2][1][0] = face.fullcube[2][2][1]
	face.fullcube[2][2][1] = face.fullcube[2][1][2]
	face.fullcube[2][1][2] = temp

	temp = face.fullcube[1][0][0]
	face.fullcube[1][0][0] = face.fullcube[0][0][0]
	face.fullcube[0][0][0] = face.fullcube[5][2][2]
	face.fullcube[5][2][2] = face.fullcube[4][0][0]
	face.fullcube[4][0][0] = temp

	temp = face.fullcube[1][1][0]
	face.fullcube[1][1][0] = face.fullcube[0][1][0]
	face.fullcube[0][1][0] = face.fullcube[5][1][2]
	face.fullcube[5][1][2] = face.fullcube[4][1][0]
	face.fullcube[4][1][0] = temp

	temp = face.fullcube[1][2][0]
	face.fullcube[1][2][0] = face.fullcube[0][2][0]
	face.fullcube[0][2][0] = face.fullcube[5][0][2]
	face.fullcube[5][0][2] = face.fullcube[4][2][0]
	face.fullcube[4][2][0] = temp

	return face.fullcube
}

func (face *Face) rotateBack() [][][]string {

	fmt.Println("rotating back side")
	temp := face.fullcube[5][0][0]
	face.fullcube[5][0][0] = face.fullcube[5][0][2]
	face.fullcube[5][0][2] = face.fullcube[5][2][2]
	face.fullcube[5][2][2] = face.fullcube[5][2][0]
	face.fullcube[5][2][0] = temp

	temp = face.fullcube[5][0][1]
	face.fullcube[5][0][1] = face.fullcube[5][1][2]
	face.fullcube[5][1][2] = face.fullcube[5][2][1]
	face.fullcube[5][2][1] = face.fullcube[5][1][0]
	face.fullcube[5][1][0] = temp

	temp = face.fullcube[0][0][0]
	face.fullcube[0][0][0] = face.fullcube[2][2][0]
	face.fullcube[2][2][0] = face.fullcube[4][2][2]
	face.fullcube[4][2][2] = face.fullcube[3][0][2]
	face.fullcube[3][0][2] = temp

	temp = face.fullcube[0][0][1]
	face.fullcube[0][0][1] = face.fullcube[2][1][0]
	face.fullcube[2][1][0] = face.fullcube[4][2][1]
	face.fullcube[4][2][1] = face.fullcube[3][1][2]
	face.fullcube[3][1][2] = temp

	temp = face.fullcube[0][0][2]
	face.fullcube[0][0][2] = face.fullcube[2][0][0]
	face.fullcube[2][0][0] = face.fullcube[4][2][0]
	face.fullcube[4][2][0] = face.fullcube[3][2][2]
	face.fullcube[3][2][2] = temp

	return face.fullcube
}
func (face *Face) rotateBackNoPrint() [][][]string {
	temp := face.fullcube[5][0][0]
	face.fullcube[5][0][0] = face.fullcube[5][0][2]
	face.fullcube[5][0][2] = face.fullcube[5][2][2]
	face.fullcube[5][2][2] = face.fullcube[5][2][0]
	face.fullcube[5][2][0] = temp

	temp = face.fullcube[5][0][1]
	face.fullcube[5][0][1] = face.fullcube[5][1][2]
	face.fullcube[5][1][2] = face.fullcube[5][2][1]
	face.fullcube[5][2][1] = face.fullcube[5][1][0]
	face.fullcube[5][1][0] = temp

	temp = face.fullcube[0][0][0]
	face.fullcube[0][0][0] = face.fullcube[2][2][0]
	face.fullcube[2][2][0] = face.fullcube[4][2][2]
	face.fullcube[4][2][2] = face.fullcube[3][0][2]
	face.fullcube[3][0][2] = temp

	temp = face.fullcube[0][0][1]
	face.fullcube[0][0][1] = face.fullcube[2][1][0]
	face.fullcube[2][1][0] = face.fullcube[4][2][1]
	face.fullcube[4][2][1] = face.fullcube[3][1][2]
	face.fullcube[3][1][2] = temp

	temp = face.fullcube[0][0][2]
	face.fullcube[0][0][2] = face.fullcube[2][0][0]
	face.fullcube[2][0][0] = face.fullcube[4][2][0]
	face.fullcube[4][2][0] = face.fullcube[3][2][2]
	face.fullcube[3][2][2] = temp

	return face.fullcube
}

func (face *Face) rotateFront() [][][]string {

	fmt.Println("rotating front side")
	temp := face.fullcube[1][0][0]
	face.fullcube[1][0][0] = face.fullcube[1][2][0]
	face.fullcube[1][2][0] = face.fullcube[1][2][2]
	face.fullcube[1][2][2] = face.fullcube[1][0][2]
	face.fullcube[1][0][2] = temp

	temp = face.fullcube[1][0][1]
	face.fullcube[1][0][1] = face.fullcube[1][1][0]
	face.fullcube[1][1][0] = face.fullcube[1][2][1]
	face.fullcube[1][2][1] = face.fullcube[1][1][2]
	face.fullcube[1][1][2] = temp

	temp = face.fullcube[0][2][0]
	face.fullcube[0][2][0] = face.fullcube[2][2][2]
	face.fullcube[2][2][2] = face.fullcube[4][0][2]
	face.fullcube[4][0][2] = face.fullcube[3][0][0]
	face.fullcube[3][0][0] = temp

	temp = face.fullcube[0][2][1]
	face.fullcube[0][2][1] = face.fullcube[2][1][2]
	face.fullcube[2][1][2] = face.fullcube[4][0][1]
	face.fullcube[4][0][1] = face.fullcube[3][1][0]
	face.fullcube[3][1][0] = temp

	temp = face.fullcube[0][2][2]
	face.fullcube[0][2][2] = face.fullcube[2][0][2]
	face.fullcube[2][0][2] = face.fullcube[4][0][0]
	face.fullcube[4][0][0] = face.fullcube[3][2][0]
	face.fullcube[3][2][0] = temp

	return face.fullcube
}

func (face *Face) rotateFrontNoPrint() [][][]string {

	temp := face.fullcube[1][0][0]
	face.fullcube[1][0][0] = face.fullcube[1][2][0]
	face.fullcube[1][2][0] = face.fullcube[1][2][2]
	face.fullcube[1][2][2] = face.fullcube[1][0][2]
	face.fullcube[1][0][2] = temp

	temp = face.fullcube[1][0][1]
	face.fullcube[1][0][1] = face.fullcube[1][1][0]
	face.fullcube[1][1][0] = face.fullcube[1][2][1]
	face.fullcube[1][2][1] = face.fullcube[1][1][2]
	face.fullcube[1][1][2] = temp

	temp = face.fullcube[0][2][0]
	face.fullcube[0][2][0] = face.fullcube[2][2][2]
	face.fullcube[2][2][2] = face.fullcube[4][0][2]
	face.fullcube[4][0][2] = face.fullcube[3][0][0]
	face.fullcube[3][0][0] = temp

	temp = face.fullcube[0][2][1]
	face.fullcube[0][2][1] = face.fullcube[2][1][2]
	face.fullcube[2][1][2] = face.fullcube[4][0][1]
	face.fullcube[4][0][1] = face.fullcube[3][1][0]
	face.fullcube[3][1][0] = temp

	temp = face.fullcube[0][2][2]
	face.fullcube[0][2][2] = face.fullcube[2][0][2]
	face.fullcube[2][0][2] = face.fullcube[4][0][0]
	face.fullcube[4][0][0] = face.fullcube[3][2][0]
	face.fullcube[3][2][0] = temp

	return face.fullcube
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func stringInSliceCount(a string, list []string) int {
	var count = 0
	for _, b := range list {
		if b == a {
			count += 1
		}
	}
	return count
}

func stringInSlice2D(a string, list [][]string) bool {
	for _, b := range list {
		for _, c := range b {
			if c == a {
				return true
			}
		}
	}

	return false
}

func (face *Face) whiteCross() [][][]string {

	if face.fullcube[0][0][1] == "w" && face.fullcube[0][1][2] == "w" && face.fullcube[0][2][1] == "w" && face.fullcube[0][1][0] == "w" {
		fmt.Println(" have created a daisy")
	}

	for face.fullcube[4][0][1] != "w" || face.fullcube[4][1][2] != "w" || face.fullcube[4][2][1] != "w" || face.fullcube[4][1][0] != "w" {

		for face.fullcube[4][0][1] != face.fullcube[4][1][1] {
			if face.fullcube[1][0][1] != face.fullcube[1][1][1] ||
				face.fullcube[0][2][1] != "w" {
				face.rotatetop()
			}
			if face.fullcube[1][0][1] == face.fullcube[1][1][1] {
				for face.fullcube[4][0][1] != "w" {
					face.rotateFront()
				}
				fmt.Println("green white side solved")
			}
		}

		for face.fullcube[4][1][2] != face.fullcube[4][1][1] {
			if face.fullcube[3][0][1] != face.fullcube[3][1][1] ||
				face.fullcube[0][1][2] != "w" {
				face.rotatetop()
			}
			if face.fullcube[3][0][1] == face.fullcube[3][1][1] {
				for face.fullcube[4][1][2] != "w" {
					face.rotateRight()
				}
				fmt.Println("orange side solved")
			}
		}

		for face.fullcube[4][1][0] != face.fullcube[4][1][1] {
			if face.fullcube[2][0][1] != face.fullcube[2][1][1] || face.fullcube[0][1][0] != "w" {
				face.rotatetop()
			}
			if face.fullcube[2][0][1] == face.fullcube[2][1][1] {
				for face.fullcube[4][1][0] != "w" {
					face.rotateLeft()
				}
				fmt.Println("red white side solved")
			}
		}

		for face.fullcube[4][2][1] != face.fullcube[4][1][1] {
			if face.fullcube[5][0][1] != face.fullcube[5][1][1] ||
				face.fullcube[0][0][1] != "w" {
				face.rotatetop()
			}
			if face.fullcube[5][0][1] == face.fullcube[5][1][1] {
				for face.fullcube[4][2][1] != "w" {
					face.rotateBack()
				}
				fmt.Println("blue white side solved")
			}
		}

	}
	fmt.Println("white cross solved")
	return face.fullcube

}

func (face *Face) whiteFaceBottom() [][][]string {
	if face.fullcube[4][0][1] == "w" && face.fullcube[4][1][1] == "w" && face.fullcube[4][1][2] == "w" && face.fullcube[4][2][1] == "w" {
		fmt.Println("White cross completed")
	}
	//topFrontCorners := []string{face.fullcube[1][0][0], face.fullcube[1][0][2], face.fullcube[3][0][0], face.fullcube[3][0][2],
	//	face.fullcube[5][0][0], face.fullcube[5][0][2], face.fullcube[2][0][0], face.fullcube[2][0][2]}

	//|| face.fullcube[4][0][2] != "w" || face.fullcube[4][2][0] != "w" || face.fullcube[4][2][2] != "w"
	for face.fullcube[4][0][0] != "w" || face.fullcube[4][0][2] != "w" || face.fullcube[4][2][2] != "w" || face.fullcube[4][2][0] != "w" {
		//if there is a white in the toprow
		for stringInSlice("w", face.topFrontCorners) == true {
			for i := range face.topFrontCorners {
				println(i)
				//println(pos2(face.topFrontCorners[i]))
				if face.topFrontCorners[i] == "w" && i%2 != 0 {
					//fmt.Println(pos2(topFrontCorners[i]))

					for face.fullcube[2][0][2] != face.topFrontCorners[i] {
						face.topFaceCounter()
					}
					for face.fullcube[1][0][0] != face.fullcube[1][1][1] || face.fullcube[2][0][2] != face.topFrontCorners[i] {
						face.topFaceCounter()
						face.RotateCubeClockWise()
					}
					face.leftTrigger()

				}
				if face.topFrontCorners[i] == "w" && i%2 == 0 {
					for face.fullcube[3][0][0] != face.topFrontCorners[i] {
						face.topFaceCounter()
					}
					for face.fullcube[1][0][2] != face.fullcube[1][1][1] || face.fullcube[3][0][0] != face.topFrontCorners[i] {
						face.topFaceCounter()
						face.RotateCubeClockWise()
					}
					face.rightTrigger()
				}
				face.updateAllAreas()

			}
		}
		for stringInSlice("w", face.bottomRowCorners) == true {
			for i := range face.bottomRowCorners {
				face.setBottomRow()
				println(i)

				if face.bottomRowCorners[i] == "w" && i%2 == 0 {

					for face.fullcube[3][2][0] != face.bottomRowCorners[i] {
						face.RotateCubeClockWise()
					}
					if face.fullcube[3][2][0] == face.bottomRowCorners[i] {
						face.rightTrigger()
					}
				}
				if face.bottomRowCorners[i] == "w" && i%2 != 0 {

					for face.fullcube[2][2][2] != face.bottomRowCorners[i] {
						face.RotateCubeClockWise()
					}
					if face.fullcube[2][2][2] == face.bottomRowCorners[i] {
						face.leftTrigger()
					}
				}
				face.updateAllAreas()
			}
		}

		for stringInSlice("w", face.topFaceCorners) == true {
			for i := range face.topFaceCorners {
				face.setTopFaceCorners()
				println(i)

				if face.topFaceCorners[i] == "w" && i%2 == 0 {
					for face.fullcube[4][0][0] != "w" {
						face.RotateCubeClockWise()
					}
					for face.fullcube[0][2][0] != "w" {
						face.rotatetop()
					}

					face.leftFaceCounter()
					face.rotatetop()
					face.rotateLeft()
				}

				if face.topFaceCorners[i] == "w" && i%2 != 0 {
					for face.fullcube[4][0][2] != "w" {
						face.RotateCubeClockWise()
					}
					for face.fullcube[0][2][2] != "w" {
						face.rotatetop()
					}

					face.rotateRight()
					face.topFaceCounter()
					face.rightFaceCounter()
				}
				face.updateAllAreas()

			}

		}
	}
	return face.fullcube
}

func (face *Face) F2L() [][][]string {

	for face.fullcube[1][1][0] != face.fullcube[1][1][1] || face.fullcube[1][1][1] !=
		face.fullcube[1][1][2] || face.fullcube[3][1][0] != face.fullcube[3][1][1] || face.fullcube[3][1][1] !=
		face.fullcube[3][1][2] || face.fullcube[2][1][0] != face.fullcube[2][1][1] || face.fullcube[2][1][2] !=
		face.fullcube[2][1][1] || face.fullcube[5][1][0] != face.fullcube[5][1][1] || face.fullcube[5][1][2] !=
		face.fullcube[5][1][1] {

		for i := range face.topandFrontFaceCentres {
			println(i)

			for stringInSlice("y", face.topandFrontFaceCentres[i]) == false {
				face.updateAllAreas()
				for face.fullcube[1][0][1] != face.topandFrontFaceCentres[i][0] || face.fullcube[0][2][1] == "y" {
					face.topFaceCounter()
				}
				for face.fullcube[1][0][1] != face.fullcube[1][1][1] {
					face.topFaceCounter()
					face.RotateCubeClockWise()
				}
				if face.fullcube[0][2][1] == face.fullcube[3][1][1] {
					face.rotatetop()
					face.rightTrigger()
					for face.fullcube[1][0][0] != face.fullcube[1][1][1] {
						face.topFaceCounter()
						face.RotateCubeClockWise()
					}
					face.leftTrigger()
				}
				if face.fullcube[0][2][1] == face.fullcube[2][1][1] {
					face.topFaceCounter()
					face.leftTrigger()
					for face.fullcube[1][0][2] != face.fullcube[1][1][1] {
						face.rotatetop()
						face.antiClockRotate()
					}
					face.rightTrigger()
				}
				face.updateAllAreas()
			}

			face.updateAllAreas()

		}

		if (face.fullcube[1][1][0] != face.fullcube[1][1][1] || face.fullcube[1][1][1] !=
			face.fullcube[1][1][2] || face.fullcube[3][1][0] != face.fullcube[3][1][1] || face.fullcube[3][1][1] !=
			face.fullcube[3][1][2] || face.fullcube[2][1][0] != face.fullcube[2][1][1] || face.fullcube[2][1][2] !=
			face.fullcube[2][1][1] || face.fullcube[5][1][0] != face.fullcube[5][1][1] || face.fullcube[5][1][2] !=
			face.fullcube[5][1][1]) && stringInSlice2D("y", face.topandFrontFaceCentres) == true {

			for face.fullcube[1][1][2] == face.fullcube[1][1][1] {
				face.RotateCubeClockWise()
			}
			face.rightTrigger()

			for face.fullcube[1][0][0] != face.fullcube[1][1][1] {
				face.topFaceCounter()
				face.RotateCubeClockWise()
			}
			face.leftTrigger()

			face.updateAllAreas()

		}

	}
	return face.fullcube

}

func (face *Face) topCross() [][][]string {

	for face.fullcube[0][0][1] != face.fullcube[0][1][1] || face.fullcube[0][1][1] != face.fullcube[0][1][2] ||
		face.fullcube[0][2][1] != face.fullcube[0][1][1] || face.fullcube[0][1][0] != face.fullcube[0][1][1] {

		if face.fullcube[0][0][1] == face.fullcube[0][1][1] && face.fullcube[0][2][1] == face.fullcube[0][1][1] {
			face.RotateCubeClockWise()
		}

		if face.fullcube[0][1][0] == face.fullcube[0][1][1] && face.fullcube[0][2][1] == face.fullcube[0][1][1] ||
			face.fullcube[0][1][2] == face.fullcube[0][1][1] && face.fullcube[0][2][1] == face.fullcube[0][1][1] {
			face.RotateCubeClockWise()
		}

		if face.fullcube[0][0][1] == face.fullcube[0][1][1] && face.fullcube[0][1][2] == face.fullcube[0][1][1] ||
			face.fullcube[0][1][2] == face.fullcube[0][1][1] && face.fullcube[0][2][1] == face.fullcube[0][1][1] {
			face.RotateCubeClockWise()
		}

		if face.fullcube[0][1][0] == face.fullcube[0][1][1] && face.fullcube[0][1][2] == face.fullcube[0][1][1] ||
			face.fullcube[0][0][1] == face.fullcube[0][1][1] && face.fullcube[0][1][0] == face.fullcube[0][1][1] ||
			face.fullcube[0][1][1] == "y" && face.fullcube[0][0][1] != face.fullcube[0][1][1] &&
				face.fullcube[0][1][2] != face.fullcube[0][1][1] && face.fullcube[0][2][1] != face.fullcube[0][1][1] {
			face.topCrossMove()
		}

		//face.fullcube[0][1][0] == face.fullcube[0][1][1] && face.fullcube[0][1][2] == face.fullcube[0][1][1] || face.fullcube[0][0][1] == face.fullcube[0][1][1] && face.fullcube[0][1][0] == face.fullcube[0][1][1] || face.fullcube[0][1][1]=="y" && face.fullcube[0][0][1]!=face.fullcube[0][1][1] &&
		//	face.fullcube[0][1][2]!=face.fullcube[0][1][1] && face.fullcube[0][2][1]!=face.fullcube[0][1][1]
		//for face.fullcube[0][0][1]==face.fullcube[0][1][1] && face.fullcube[0][1][0]==face.fullcube[0][1][1]||
		//	face.fullcube[0][1][0]==face.fullcube[0][1][1] && face.fullcube[0][1][2]==face.fullcube[0][1][1]||
		//	face.fullcube[0][0][1]==face.fullcube[0][1][1] && face.fullcube[0][1][0]==face.fullcube[0][1][1]

	}

	return face.fullcube
}

func (face *Face) topFaceFull() [][][]string {

	var t = face.fullcube[0][1][1]
	for face.fullcube[0][1][1] != face.fullcube[0][0][0] || face.fullcube[0][1][2] != face.fullcube[0][0][2] ||
		face.fullcube[0][2][0] != face.fullcube[0][1][1] || face.fullcube[0][2][2] != face.fullcube[0][1][1] {

		if face.fullcube[0][0][0] != face.fullcube[0][1][1] && face.fullcube[0][0][2] != face.fullcube[0][1][1] &&
			face.fullcube[0][2][0] != face.fullcube[0][1][1] && face.fullcube[0][2][2] != face.fullcube[0][1][1] {

			for face.fullcube[2][0][2] != face.fullcube[0][1][1] {
				face.RotateCubeClockWise()
			}
			face.topFaceFullMove()
		}

		if face.fullcube[0][0][0] == t && face.fullcube[0][0][2] == t && face.fullcube[0][2][0] != t && face.fullcube[0][2][2] != t ||
			face.fullcube[0][0][2] == t && face.fullcube[0][2][2] == t && face.fullcube[0][0][0] != t && face.fullcube[0][2][0] != t ||
			face.fullcube[0][2][0] == t && face.fullcube[0][2][2] == t && face.fullcube[0][0][0] != t && face.fullcube[0][0][2] != t ||
			face.fullcube[0][0][0] == t && face.fullcube[0][2][0] == t && face.fullcube[0][0][2] != t && face.fullcube[0][2][2] != t {

			for face.fullcube[0][0][0] != t && face.fullcube[0][0][2] != t {
				face.RotateCubeClockWise()
			}
			face.topFaceFullMove()
		}

		face.updateAllAreas()

		if stringInSliceCount("y", face.topFaceCorners) == 1 {

			for face.fullcube[0][2][0] != t {
				face.RotateCubeClockWise()
			}
			face.topFaceFullMove()
		}
		face.updateAllAreas()

	}

	return face.fullcube

}

func (face *Face) topCrossMove() {

	face.rotateFront()
	face.rotatetop()
	face.rotateRight()
	face.topFaceCounter()
	face.rightFaceCounter()
	face.frontFaceCounter()
}

func (face *Face) topFaceFullMove() {

	face.rotateRight()
	face.rotatetop()
	face.rightFaceCounter()
	face.rotatetop()
	face.rotateRight()
	face.rotatetop()
	face.rotatetop()
	face.rightFaceCounter()

}

//var i, j, k int
func (face *Face) daisy() [][][]string {
	fmt.Println("Now we have to try and get a daisy")

	//dcube :=  [][][] string{}
	//face.fullcube=dcube
	newCube := []string{face.fullcube[0][0][1], face.fullcube[0][1][2], face.fullcube[0][1][0], face.fullcube[0][2][1]}

	var temp = "w"
	for face.fullcube[0][0][1] != temp || face.fullcube[0][1][2] != temp || face.fullcube[0][2][1] != temp || face.fullcube[0][1][0] != temp {
		fmt.Println("daisy not solved")

		if face.fullcube[0][1][0] != "w" && face.fullcube[0][2][1] != "w" {
			fmt.Println("condition 1")
			break
		}

		for range newCube {

		}
	}

	fmt.Println("solved daisy")
	return face.fullcube
}

//doing the inverse of a face movement is doing it normally 3 times

func (face *Face) leftFaceCounter() {
	fmt.Println("left counter")
	face.rotateLeftNoPrint()
	face.rotateLeftNoPrint()
	face.rotateLeftNoPrint()
}

func (face *Face) rightFaceCounter() {
	fmt.Println("right counter")

	face.rotateRightNoPrint()
	face.rotateRightNoPrint()
	face.rotateRightNoPrint()
}

func (face *Face) frontFaceCounter() {
	fmt.Println("front counter")

	face.rotateFrontNoPrint()
	face.rotateFrontNoPrint()
	face.rotateFrontNoPrint()
}

func (face *Face) backFaceCounter() {
	fmt.Println("back counter")

	face.rotateBackNoPrint()
	face.rotateBackNoPrint()
	face.rotateBackNoPrint()
}
func (face *Face) topFaceCounter() {
	fmt.Println("top counter")

	face.rotatetopNoPrint()
	face.rotatetopNoPrint()
	face.rotatetopNoPrint()
}

//func (face *Face) rotateBottom() {
//
//	temp:=face.fullcube[4][0][0]
//	face.fullcube
//}
func (face *Face) RotateCubeClockWise() {

	fmt.Println("rotating cube clockwise ")
	//changing top
	temp := face.fullcube[0][0][0]
	//e.g. 020 will become the first part at the top
	face.fullcube[0][0][0] = face.fullcube[0][2][0]
	face.fullcube[0][2][0] = face.fullcube[0][2][2]
	face.fullcube[0][2][2] = face.fullcube[0][0][2]
	face.fullcube[0][0][2] = temp

	temp = face.fullcube[0][0][1]
	face.fullcube[0][0][1] = face.fullcube[0][1][0]
	face.fullcube[0][1][0] = face.fullcube[0][2][1]
	face.fullcube[0][2][1] = face.fullcube[0][1][2]
	face.fullcube[0][1][2] = temp

	//change bottom
	temp = face.fullcube[4][0][0]
	face.fullcube[4][0][0] = face.fullcube[4][0][2]
	face.fullcube[4][0][2] = face.fullcube[4][2][2]
	face.fullcube[4][2][2] = face.fullcube[4][2][0]
	face.fullcube[4][2][0] = temp

	temp2 := face.fullcube[1]
	face.fullcube[1] = face.fullcube[3]
	face.fullcube[3] = face.fullcube[5]
	face.fullcube[5] = face.fullcube[2]
	face.fullcube[2] = temp2

}

func (face *Face) RotateCubeClockWiseNoPrint() {

	//changing top
	temp := face.fullcube[0][0][0]
	//e.g. 020 will become the first part at the top
	face.fullcube[0][0][0] = face.fullcube[0][2][0]
	face.fullcube[0][2][0] = face.fullcube[0][2][2]
	face.fullcube[0][2][2] = face.fullcube[0][0][2]
	face.fullcube[0][0][2] = temp

	temp = face.fullcube[0][0][1]
	face.fullcube[0][0][1] = face.fullcube[0][1][0]
	face.fullcube[0][1][0] = face.fullcube[0][2][1]
	face.fullcube[0][2][1] = face.fullcube[0][1][2]
	face.fullcube[0][1][2] = temp

	//change bottom
	temp = face.fullcube[4][0][0]
	face.fullcube[4][0][0] = face.fullcube[4][0][2]
	face.fullcube[4][0][2] = face.fullcube[4][2][2]
	face.fullcube[4][2][2] = face.fullcube[4][2][0]
	face.fullcube[4][2][0] = temp

	temp2 := face.fullcube[1]
	face.fullcube[1] = face.fullcube[3]
	face.fullcube[3] = face.fullcube[5]
	face.fullcube[5] = face.fullcube[2]
	face.fullcube[2] = temp2
}
func (face *Face) antiClockRotate() {

	fmt.Println("Rotatingcube anticlock wise")
	face.RotateCubeClockWiseNoPrint()
	face.RotateCubeClockWiseNoPrint()
	face.RotateCubeClockWiseNoPrint()
}

func (face *Face) leftTrigger() {

	face.leftFaceCounter()
	face.topFaceCounter()
	face.rotateLeft()
}

func (face *Face) rightTrigger() {

	face.rotateRight()
	face.rotatetop()
	face.rightFaceCounter()
}
